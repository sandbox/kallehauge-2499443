<?php

/**
 * @file
 * Custom content type.
 *
 * This content type is nothing more than a title and a body that is entered
 * by the user and run through standard filters. The information is stored
 * right in the config, so each custom content is unique.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('New custom content definition (i18n)'),
  'no title override' => TRUE,
  'defaults' => array(array('general' => array('admin_title' => ''), 'languages' => array())),
  'js' => array('misc/autocomplete.js', 'misc/textarea.js', 'misc/collapse.js'),
  // Make sure the edit form is only used for some subtypes.
  'description' => t('Create a completely custom piece of HTML content.'),
  'edit form' => 'i18n_custom_definition_i18n_custom_definition_pane_content_type_edit_form',
  'all contexts' => TRUE,
  'edit text' => t('Edit'),
  'single' => TRUE,
  'icon' => 'icon_block_custom.png',
  'category' => t('Custom'),
  'top level' => TRUE,
);

/**
 * Output function for the 'custom' content type. Outputs a custom
 * based on the module and delta supplied in the configuration.
 */
function i18n_custom_definition_i18n_custom_definition_pane_content_type_render($subtype, $conf, $args, $contexts) {
  static $delta = 0;
  $block          = new stdClass();
  $block->subtype = ++$delta;

  if ($configuration = variable_get('i18n_custom_definition_' . $conf['general']['pane_machine_name'], FALSE)) {
    global $language;
    if (!empty($configuration['languages'][$language->language])) {
      $block->title = filter_xss_admin($configuration['languages'][$language->language]['title']);

      $content = $configuration['languages'][$language->language]['body']['value'];
      if (!empty($contexts) && !empty($configuration['languages'][$language->language]['substitute'])) {
        $content = ctools_context_keyword_substitute($content, array(), $contexts);
      }
      $block->content = check_markup($content, $configuration['languages'][$language->language]['body']['format']);
    }
  }

  return $block;
}

/**
 * Callback to provide the administrative title of the custom content.
 */
function i18n_custom_definition_i18n_custom_definition_pane_content_type_admin_title($subtype, $conf) {
  $output = t('i18n custom content');
  $title = !empty($conf['general']['admin_title']) ? $conf['general']['admin_title'] : NULL;
  if ($title) {
    $output = t('i18n custom content definition: @title', array('@title' => $title));
  }

  return $output;
}

/**
 * Callback to provide administrative info. In this case we'll render the
 * content as long as it's not PHP, which is too risky to render here.
 */
function i18n_custom_definition_i18n_custom_definition_pane_content_type_admin_info($subtype, $conf) {
  $block = new stdClass();
  $block->title = filter_xss_admin($conf['general']['admin_title']);
  return $block;
}

/**
 * Returns an edit form for the custom type.
 */
function i18n_custom_definition_i18n_custom_definition_pane_content_type_edit_form($form, &$form_state) {
  // Get data.
  $general_settings = $form_state['conf']['general'];
  if (!empty($form_state['conf']['general']['pane_machine_name'])) {
    $languages_settings = variable_get('i18n_custom_definition_' . $form_state['conf']['general']['pane_machine_name'], FALSE);
    $language_exists = TRUE;
  }
  else {
    $language_exists = FALSE;
  }

  $form_all = $form;
  $languages = language_list();
  $form_all['languages_tabs'] = array(
    '#type' => 'vertical_tabs',
  );

  $form_all['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
    '#group' => 'languages_tabs',
    '#weight' => -99,
  );
  $form_all['general']['admin_title'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($general_settings['admin_title']) ? $general_settings['admin_title'] : '',
    '#title' => t('Administrative title'),
    '#description' => t('This title will be used administratively to identify this pane. If blank, the regular title will be used.'),
  );
  $form_all['general']['pane_machine_name'] = array(
    '#title' => t('Pane machine name'),
    '#type' => 'textfield',
    '#default_value' => isset($general_settings['pane_machine_name']) ? $general_settings['pane_machine_name'] : '',
    '#description' => t('The machine name of this pane.'),
    '#required' => TRUE,
  );
  $form_all['languages'] = array(
    '#tree' => TRUE,
  );

  foreach ($languages as $langcode => $language) {
    $form = array();
    if ($language_exists) {
      $settings = $languages_settings['languages'][$langcode];
    }

    $form['title'] = array(
      '#type' => 'textfield',
      '#default_value' => $language_exists ? $settings['title'] : '',
      '#title' => t('Title'),
    );

    $form['body'] = array(
      '#type' => 'text_format',
      '#title' => t('Body'),
      '#default_value' => $language_exists ? $settings['body']['value'] : '',
      '#format' => isset($settings['body']['format']) ? $settings['body']['format'] : filter_default_format(),
    );

    if (!empty($form_state['contexts'])) {
      // Set extended description if both CCK and Token modules are enabled, notifying of unlisted keywords
      if (module_exists('content') && module_exists('token')) {
        $description = t('If checked, context keywords will be substituted in this content. Note that CCK fields may be used as keywords using patterns like <em>%node:field_name-formatted</em>.');
      }
      elseif (!module_exists('token')) {
        $description = t('If checked, context keywords will be substituted in this content. More keywords will be available if you install the Token module, see http://drupal.org/project/token.');
      }
      else {
        $description = t('If checked, context keywords will be substituted in this content.');
      }

      $form['substitute'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use context keywords'),
        '#description' => $description,
        '#default_value' => !empty($settings['substitute']),
      );
      $form['contexts'] = array(
        '#title' => t('Substitutions'),
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      $rows = array();
      foreach ($form_state['contexts'] as $context) {
        foreach (ctools_context_get_converters('%' . check_plain($context->keyword) . ':', $context) as $keyword => $title) {
          $rows[] = array(
            check_plain($keyword),
            t('@identifier: @title', array('@title' => $title, '@identifier' => $context->identifier)),
          );
        }
      }
      $header = array(t('Keyword'), t('Value'));
      $form['contexts']['context'] = array(
        '#markup' => theme('table', array('header' => $header, 'rows' => $rows)),
        '#group' => 'form',
      );
    }
    $form_all['languages'][$langcode] = array(
      '#type' => 'fieldset',
      '#title' => $language->name,
      '#collapsible' => FALSE,
      '#tree' => TRUE,
      '#group' => 'languages_tabs'
    ) + $form;
  }

  return $form_all;
}

/**
 * The validate form to ensure the custom content data is okay.
 */
function i18n_custom_definition_i18n_custom_definition_pane_content_type_edit_form_validate(&$form, &$form_state) {
}

/**
 * The submit form stores the data in $conf.
 */
function i18n_custom_definition_i18n_custom_definition_pane_content_type_edit_form_submit($form, &$form_state) {
  // Save general in the conf so we can use it in the admin info hooks.
  $form_state['conf']['general'] = $form_state['values']['general'];
  // Save the content in a variable, so we can export it without the content.
  variable_set('i18n_custom_definition_' . $form_state['values']['general']['pane_machine_name'], ['languages' => $form_state['values']['languages']]);
}
